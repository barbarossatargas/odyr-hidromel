import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'

import Vuetify from 'vuetify'
import vueSmoothScroll from 'vue2-smooth-scroll'

import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
Vue.use(vueSmoothScroll)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
